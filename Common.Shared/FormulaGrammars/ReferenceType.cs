﻿namespace Common.FormulaGrammars
{
    /// <summary>
    /// 引用类型
    /// </summary>
    public enum ReferenceType
    {
        UserDefinedName,
        RefError
    }
}
