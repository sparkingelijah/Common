﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Interfaces
{
    /// <summary>
    /// 处理类型
    /// </summary>
    public interface IHandlerType
    {
        string HandlerType { get; }
    }
}
