﻿namespace Common.Interfaces
{
    /// <summary>
    /// 标记实体允许执行批量写入(PG Copy)
    /// </summary>
    public interface IBatchInsertSupport
    {
    }
}