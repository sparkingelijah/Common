﻿using System;
using System.Text.Json.Serialization;

namespace Common.Dtos
{
    public class LastReadItem
    {
        [JsonPropertyName("Date")]
        public DateTime Date { get; set; }

        [JsonPropertyName("LastReadLine")]
        public int LastReadLine { get; set; }
    }
}
