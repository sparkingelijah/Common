﻿using System;

namespace Common.Dtos
{
    public class LogRowDto
    {
        public DateTime Time { get; set; }

        public string LogTypeP {  get; set; }

        public string Content { get; set; }

        public string ProjectId { get; set; }


    }
}
