﻿using System.Collections.Generic;

namespace Common.Dtos
{
    public class LastReadInfo
    {
        public List<LastReadItem> LastReadItems { get; set; }=new List<LastReadItem> ();
    }
}
