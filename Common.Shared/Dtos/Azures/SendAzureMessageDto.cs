﻿using System.ComponentModel.DataAnnotations;

namespace Common.Dtos
{
    /// <summary>
    /// 发送消息对象
    /// </summary>
    public class SendAzureMessageDto
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string Conn { get; set; }

        /// <summary>
        /// Azure队列名称
        /// </summary>
        [Required]
        public string QueueName { get; set; }

        /// <summary>
        /// Azure队列名称
        /// </summary>
        [Required]
        public string Json { get; set; }
    }
}
