﻿namespace Common.Dtos
{
    /// <summary>
    /// 消息接收结果返回对象
    /// </summary>
    public class RecieveResultDto
    {
        /// <summary>
        /// 异步返回对象
        /// </summary>
        public SyncMessageObjectDto Syn { get; set; }

        /// <summary>
        /// 应答
        /// </summary>
        public AckInfo Ack { get; set; }
    }

    /// <summary>
    /// 应答
    /// </summary>
    public class AckInfo
    {
        public string FileName { get; set; }

        public string Status { get; set; }

        public string Result { get; set; }

        public string Msg { get; set; }
    }
}
