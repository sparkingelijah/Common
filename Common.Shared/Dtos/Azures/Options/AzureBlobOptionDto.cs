﻿using Common.Enums.External;
using Common.Helpers;
using Volo.Abp.Account.Settings;

namespace Common.Dtos
{
    /// <summary>
    /// AzureBlob 配置类
    /// </summary>
    public class AzureBlobOptionDto
    {
        public string BlobReference { get; set; }

        public string BlobDownloadReference { get; set; }

    }
}
