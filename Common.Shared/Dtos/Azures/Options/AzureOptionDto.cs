﻿using Common.Enums.External;
using Common.Helpers;
using Nest;
using Volo.Abp.Account.Settings;

namespace Common.Dtos
{
    /// <summary>
    /// AzureOption 配置类
    /// </summary>
    public class AzureOptionDto 
    {
        /// <summary>
        /// http类型
        /// </summary>
        public HttpType HttpType { get; set; }

        /// <summary>
        /// https or http
        /// </summary>
        public string Http { get; set; }

        /// <summary>
        /// 账号名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 账号密钥
        /// </summary>
        public string AccountKey { get; set; }

        /// <summary>
        /// 密钥解密后字符串
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        [System.Text.Json.Serialization.JsonIgnore]
        public string AccountKeyDecrypted { get { return EncodingEncryptHelper.DEncrypt(AccountKey); } }

        /// <summary>
        /// 前缀
        /// </summary>
        public string EndpointSuffix { get; set; }

        /// <summary>
        /// AzureBolb
        /// </summary>
        public AzureBlobOptionDto AzureBlob { get; set; }

        /// <summary>
        /// AzureQueue
        /// </summary>
        public AzureQueueOptionDto AzureQueues { get; set; }
    }
}
