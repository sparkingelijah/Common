﻿namespace Common.Dtos
{
    /// <summary>
    /// AzureQueue 配置类
    /// </summary>
    public class AzureQueueOptionDto
    {
        /// <summary>
        /// QueuesReference
        /// </summary>
        public string QueuesReference { get; set; }
    }
}
