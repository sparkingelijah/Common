﻿using System.ComponentModel.DataAnnotations;

namespace Common.Dtos
{
    /// <summary>
    /// 删除消息
    /// </summary>
    public class DeleteAzureMessageDto : RecieveAzureMessageDto
    {
        /// <summary>
        /// MessageId
        /// </summary>
        [Required]
        public string MessageId { get; set; }

        /// <summary>
        /// 队列消息的某个唯一标识符 用户查询删除
        /// </summary>
        [Required]
        public string PopReceipt { get; set; }
    }
}
