﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Dtos
{
    /// <summary>
    /// 消息接收结果返回对象
    /// </summary>
    public class SyncMessageObjectDto
    {
        public int Type { get; set; }

        public string ADUser { get; set; }

        public object Content { get; set; }

        public string MessageId { get; set; }
    }
}
