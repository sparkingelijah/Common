﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Dtos
{
    public class JWTTokenResult
    {
        public bool Result { get; set; }
        public string Token { get; set; }
    }
}
