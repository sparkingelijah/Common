﻿namespace Common.Dtos
{
    /// <summary>
    /// 获取企业微信部门id列表输入类
    /// </summary>
    public class GetWeixinDepartmentListInputDto
    {
        /// <summary>
        /// 部门Id 非必填
        /// </summary>
        public string DepartmentId { get; set; }

    }
}
