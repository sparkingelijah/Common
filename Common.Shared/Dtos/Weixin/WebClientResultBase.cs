﻿namespace Common.Dtos
{
    /// <summary>
    /// WebClientResultBase
    /// </summary>
    public class WebClientResultBase
    {
        /// <summary>
        /// 错误编码
        /// </summary>
        public long Errcode { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        public string Errmsg { get; set; }
        
    }
}
