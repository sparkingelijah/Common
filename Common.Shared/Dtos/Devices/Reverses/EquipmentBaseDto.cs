﻿namespace Common.Dtos
{
    /// <summary>
    /// 基础抽象类
    /// </summary>
    public abstract class EquipmentBaseDto
    {
        /// <summary>
        /// 命令名称
        /// </summary>
        public abstract string CommandName { get; }
    }
}
