﻿namespace Common.Dtos
{
    /// <summary>
    /// 添加人脸
    /// </summary>
    public class FaceDto
    {
        /// <summary>
        /// 图片唯一标识ID
        /// </summary>
        public string FaceId { get; set; }

        /// <summary>
        /// image
        /// </summary>
        public string Image { get; set; }
    }
}
