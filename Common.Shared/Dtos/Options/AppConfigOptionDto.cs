﻿namespace Common.Dtos
{
    /// <summary>
    /// App启动节点 配置类
    /// </summary>
    public class AppConfigOptionDto
    {
        /// <summary>
        /// 本机启动地址Url
        /// </summary>
        public string SelfUrl { get; set; }

        /// <summary>
        /// 是否自动升级
        /// </summary>
        public bool IsAutoUpgrade { get; set; }
    }
}
