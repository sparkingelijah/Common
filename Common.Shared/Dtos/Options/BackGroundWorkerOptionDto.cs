﻿namespace Common.Dtos
{
    /// <summary>
    /// 后台任务配置类
    /// </summary>
    public class BackGroundWorkerOptionDto
    {
        /// <summary>
        /// 是否开启后台任务总开关
        /// </summary>
        public bool IsEnable{ get; set; }

        /// <summary>
        /// 是否开启导入导出任务开关
        /// </summary>
        public bool IsEnableImExport { get; set; }

        /// <summary>
        /// 是否开启事件开关
        /// </summary>
        public bool IsEnableEvent { get; set; }

        /// <summary>
        /// 导入导出后台任务 时间间隔
        /// </summary>

        public int ImExportConsumerInterval { get; set; } = 10000;

        /// <summary>
        /// 事件任务 时间间隔
        /// </summary>

        public int EventConsumerInterval { get; set; } = 8000;

    }
}
