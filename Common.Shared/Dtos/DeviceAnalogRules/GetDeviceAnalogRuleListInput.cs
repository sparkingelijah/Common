﻿using Volo.Abp.Application.Dtos;

namespace Common.Dtos
{
    /// <summary>
    /// 获取规则数据列表
    /// </summary>
    public class GetDeviceAnalogRuleListInput : GetDeviceAnalogRuleBaseInput
    {
    }
}
