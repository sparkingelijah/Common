﻿using System.ComponentModel;

namespace Common.Enums.External
{
    /// <summary>
    /// http类型
    /// </summary>
    [Description("http类型")]
    public enum HttpType
    {
        /// <summary>
        /// Https安全协议网络请求
        /// </summary>
        [Description("HTTPS")]
        HTTPS = 0,
        /// <summary>
        /// Http标准
        /// </summary>
        [Description("HTTP")]
        HTTP = 1
    }
}
