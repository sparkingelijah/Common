﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;

namespace Common.Nacos
{
    /// <summary>
    /// 读取nacos配置文件中 dataid="EnvironmentVariables" 的环境变量配置 管理类
    /// </summary>
    public class NacosEnvironment
    {
        private static ConcurrentDictionary<string, string> _enviroments = null;
        private static object locker = new object();
        public static string NacosEnvironmentVariablesDataId = NacosHelper.GetEnviromentValue("NACOS_ENVIRONMENT_VARIABLES_DATA_ID", "EnvironmentVariables");

        /// <summary>
        /// 从nacos中获取环境变量的值
        /// </summary>
        /// <param name="key"></param>
        /// <returns>不存在时返回null</returns>
        public static string GetValue(string key)
        {
            if (_enviroments == null)
            {
                lock (locker)
                {
                    if (_enviroments == null)
                    {
                        _enviroments = new ConcurrentDictionary<string, string>();
                        if (!NacosHelper.GetNacosEnabled()) return null;//不启用Nacos

                        string environmentVariablesConfig = NacosHelper.GetConfigValue(NacosEnvironmentVariablesDataId, NacosHelper.GetNacosGroup());
                        if (string.IsNullOrEmpty(environmentVariablesConfig))
                        {
                            //当Nacos中没有环境变量的配置时，初始化空的配置
                            string initJsonData = JsonConvert.SerializeObject(new NacosEnvironmentInitModel(), Formatting.Indented);
                            NacosHelper.SetConfigValue(NacosEnvironmentVariablesDataId, NacosHelper.GetNacosGroup(), initJsonData);
                        }
                        else
                        {
                            try
                            {
                                InitNacosEnvironmentVariables(environmentVariablesConfig);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception($"从Nacos中获取EnvironmentVariables反序列失败。EnvironmentVariables:{environmentVariablesConfig}\r\n{ex.Message}");
                            }
                        }
                    }
                }
            }

            if (_enviroments.ContainsKey(key)) return _enviroments[key];
            return null;
        }

        /// <summary>
        /// 初始化Nacos环境变量
        /// </summary>
        /// <param name="environmentVariablesConfig"></param>
        public static void InitNacosEnvironmentVariables(string environmentVariablesConfig)
        {
            _enviroments = JsonConvert.DeserializeObject<ConcurrentDictionary<string, string>>(environmentVariablesConfig);
        }
    }
}
