﻿namespace Common.Nacos
{
    /// <summary>
    /// 读取nacos配置文件中 dataid="EnvironmentVariables" 的环境变量配置 初始化Model
    /// </summary>
    public class NacosEnvironmentInitModel
    {
        /// <summary>
        /// DESHeper加解密"秘钥"
        /// </summary>
        public string SECRET_KEY { get; set; }
        /// <summary>
        /// 内部系统使用短token
        /// </summary>
        public string TOKEN { get; set; }
        /// <summary>
        /// 多租户管理平台URL
        /// </summary>
        public string TENANT_MANAGER_URL { get; set; }
        /// <summary>
        /// redis连接字符串
        /// </summary>
        public string REDIS_CONNECTION { get; set; }
        /// <summary>
        /// 是否调试模式(true|false)
        /// </summary>
        public string LOGGER_ISDEBUG { get; set; }
        /// <summary>
        /// 认证服务器
        /// </summary>
        public string WEBHOST { get; set; }
        /// <summary>
        /// Egmp主项目服务器
        /// </summary>
        public string EGMP_URL { get; set; }
        /// <summary>
        /// 文件转档服务器地址
        /// </summary>
        public string DOCUMENT_EXECUTE_URL { get; set; }
        /// <summary>
        /// 在线编辑服务URL
        /// </summary>
        public string ONLY_OFFICE_DOCUMENT_SERVICE_URL { get; set; }
        /// <summary>
        /// 数据库类型 1 sqlserver 2 mysql
        /// </summary>
        public string DATABASE_TYPE { get; set; }
        /// <summary>
        /// 数据库连接地址
        /// </summary>
        public string DATABASE_CONNECTION_STRING { get; set; }
        /// <summary>
        /// MQ主机地址
        /// </summary>
        public string RABBITMQ_HOST { get; set; }
        /// <summary>
        /// MQ端口
        /// </summary>
        public string RABBITMQ_PORT { get; set; }
        /// <summary>
        /// MQ用户名
        /// </summary>
        public string RABBITMQ_USERNAME { get; set; }
        /// <summary>
        /// MQ密码
        /// </summary>
        public string RABBITMQ_PASSWORD { get; set; }
        /// <summary>
        /// MQ虚拟主机
        /// </summary>
        public string RABBITMQ_VIRTUAL_HOST { get; set; }
        /// <summary>
        /// OSS 存储平台 1 aws，2 aliyun
        /// </summary>
        public string OOS_PLATFORM { get; set; }
        /// <summary>
        /// OSS服务器地址
        /// </summary>
        public string S3_URL { get; set; }
        /// <summary>
        /// OSS服务AccessKey
        /// </summary>
        public string S3_ACCESS_KEY { get; set; }
        /// <summary>
        /// OSS服务SecretKey
        /// </summary>
        public string S3_SECRET_KEY { get; set; }
        /// <summary>
        /// OSS服务桶名称
        /// </summary>
        public string S3_BUCKET_NAME { get; set; }
        /// <summary>
        /// 是否使用固定的访问地址(true|false)
        /// </summary>
        public string S3_FORCE_PATH_STYLE { get; set; }
        /// <summary>
        /// ES命名空间
        /// </summary>
        public string ES_NAMESPACE { get; set; }
        /// <summary>
        /// ES地址
        /// </summary>
        public string ES_URL { get; set; }
        /// <summary>
        /// ES用户名
        /// </summary>
        public string ES_USERNAME { get; set; }
        /// <summary>
        /// ES用户密码
        /// </summary>
        public string ES_PWD { get; set; }
        /// <summary>
        /// 是否输出ES的Info日志(true|false)
        /// </summary>
        public string ES_LOG { get; set; }
        /// <summary>
        /// 阿里云邮件服务器url
        /// </summary>
        public string EMAIL_ENDPOINT { get; set; }
        /// <summary>
        /// 默认发邮件账号
        /// </summary>
        public string EMAIL_ACCOUNT { get; set; }
        /// <summary>
        /// 阿里云邮件服务AccessKey
        /// </summary>
        public string EMAIL_ACCESS_KEY { get; set; }
        /// <summary>
        /// 阿里云邮件服务SecretKey
        /// </summary>
        public string EMAIL_SECRET_KEY { get; set; }
        /// <summary>
        /// 系统邮件服务器
        /// </summary>
        public string SMTP_HOST { get; set; }
        /// <summary>
        /// 系统邮件发送账号
        /// </summary>
        public string Smtp_User_Account { get; set; }
        /// <summary>
        /// 系统邮件发送账号名称
        /// </summary>
        public string Smtp_User_Name { get; set; }
        /// <summary>
        /// 系统邮件发送账号密码
        /// </summary>
        public string SMTP_EMAIL_PASSWORD { get; set; }
        /// <summary>
        /// 阿里云短信签名
        /// </summary>
        public string SMS_ALIYUN_SIGNNAME { get; set; }
        /// <summary>
        /// 阿里云短信服务域名
        /// </summary>
        public string SMS_ALIYUN_ENDPOINT { get; set; }
        /// <summary>
        /// 短信验证码登录的消息模板
        /// </summary>
        public string SMS_ALIYUN_TEMPLATECODE_LOGINAUTHCODE { get; set; }
        /// <summary>
        /// 邮件验证码登录的消息模板
        /// </summary>
        public string LOGIN_VERIFICATIONCODE_EMAILTEMPLATE { get; set; }
        /// <summary>
        /// 邮件验证码登录的邮件标题
        /// </summary>
        public string LOGIN_VERIFICATIONCODE_EMAILSUBJECT { get; set; }
        /// <summary>
        /// 接收系统告警Email
        /// </summary>
        public string ALARM_TOEMAIL_ADDRESS { get; set; }
        /// <summary>
        /// Nacos服务地址
        /// </summary>
        public string NACOS_URL { get; set; }
        /// <summary>
        /// Nacos命名空间
        /// </summary>
        public string NACOS_NAMESPACE { get; set; }
        /// <summary>
        /// 集群缓存服务"分组名称"
        /// </summary>
        public string ClusterCache_GroupName { get; set; }
        /// <summary>
        /// 集群缓存服务"集群名称"
        /// </summary>
        public string ClusterCache_ClusterName { get; set; }
        /// <summary>
        /// 前缀匹配的IP
        /// </summary>
        public string ClusterCache_PreferredNetworks { get; set; }
        /// <summary>
        /// 事件总线客户端名称
        /// </summary>
        public string EVENT_BUS_CLIENT_NAME { get; set; }
        /// <summary>
        /// 事件总线交换机名称
        /// </summary>
        public string EVENT_BUS_EXCHANGE_NAME { get; set; }
        /// <summary>
        /// 百度ApiKey
        /// </summary>
        public string BAIDU_API_KEY { get; set; }
        /// <summary>
        /// 百度SecretKey
        /// </summary>
        public string BAIDU_SECRET_KEY { get; set; }
        /// <summary>
        /// 请求客户端超时时间
        /// </summary>
        public string REST_CLIENT_TIMEOUT { get; set; }
        /// <summary>
        /// 请求客户端用户账号
        /// </summary>
        public string REST_USER_ACCOUNT { get; set; }
        /// <summary>
        /// 请求客户端用户密码
        /// </summary>
        public string REST_USER_PASSWORD { get; set; }

        /// <summary>
        /// 字段唯一性验证时，分布式锁超时时间（毫秒）
        /// </summary>
        public string Lock_ValidateFieldValueUnique_Timeout { get; set; }

        /// <summary>
        /// 对象实例快照数据有效期（天）
        /// </summary>
        public string SnapshotExpireDays { get; set; }

        /// <summary>
        /// 树型对象最大层级数
        /// </summary>
        public string MaxObjectTreeDeep { get; set; }

        /// <summary>
        /// 文件临时记录保留天数
        /// </summary>
        public string TEMP_RECORD_CLEAR_BEFORE_DAYS { get; set; }

        /// <summary>
        /// 启用多租户自动执行初始化操作（含版本升级）
        /// </summary>
        public string MULTIPLETENANTAUTOINIT_ENABLED { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string SYSCODE { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string HEMONY_WEB_URL { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WEB_HOSTNAME { get; set; }
    }

}
