﻿using Microsoft.Extensions.DependencyInjection;
using Nacos.V2;
using Nacos.V2.Naming.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Common.Nacos
{
    public class NacosHelper
    {
        public static IServiceProvider ApplicationServices { get; set; }

        /// <summary>
        /// 添加Nacos监听事件
        /// </summary>
        /// <param name="dataId"></param>
        /// <param name="group"></param>
        /// <param name="listener"></param>
        /// <exception cref="Exception"></exception>
        public static void AddListener(string dataId, string group, IListener listener)
        {
            if (!NacosHelper.GetNacosEnabled()) return;//不启用Nacos

            if (ApplicationServices == null) throw new Exception("NacosHelper.ApplicationServices不能为空");
            INacosConfigService nacosConfigService = ApplicationServices.GetRequiredService<INacosConfigService>();
            nacosConfigService.AddListener(dataId, group, listener);
        }

        /// <summary>
        /// 移除Nacos监听事件
        /// </summary>
        /// <param name="dataId"></param>
        /// <param name="group"></param>
        /// <param name="listener"></param>
        /// <exception cref="Exception"></exception>
        public static void RemoveListener(string dataId, string group, IListener listener)
        {
            if (!NacosHelper.GetNacosEnabled()) return;//不启用Nacos
            if (ApplicationServices == null) throw new Exception("NacosHelper.ApplicationServices不能为空");
            INacosConfigService nacosConfigService = ApplicationServices.GetRequiredService<INacosConfigService>();
            nacosConfigService.RemoveListener(dataId, group, listener);
        }

        public static void SetConfigValue(string dataId, string group, string configdata)
        {
            if (!NacosHelper.GetNacosEnabled()) return;//不启用Nacos
            if (group == null) group = GetNacosGroup();
            var url = $"{GetNacosApiServer()}/nacos/v1/cs/configs";
            var httpClient = new HttpClient();
            try
            {
                var content = new MultipartFormDataContent();
                var encoding = System.Text.Encoding.UTF8;
                content.Add(new StringContent(dataId), "dataId");
                content.Add(new StringContent(group), "group");
                content.Add(new StringContent("json"), "type");
                content.Add(new StringContent(configdata, encoding), "content");
                HttpResponseMessage responseMessage;
                responseMessage = httpClient.PostAsync(url, content).Result;
                if (responseMessage.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception($"保存Nacos配置不成功：{url}\r\n。请求结果：{Newtonsoft.Json.JsonConvert.SerializeObject(responseMessage)}");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"保存Nacos配置失败：{url}。{ex.Message}");
            }
        }

        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <param name="dataId"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static string GetConfigValue(string dataId, string? group = null)
        {
            if (!NacosHelper.GetNacosEnabled()) return null;//不启用Nacos

            if (group == null) group = GetNacosGroup();
            //if (ApplicationServices == null) throw new Exception("NacosHelper.ApplicationServices不能为空");
            //INacosConfigService svc = ApplicationServices.GetService<INacosConfigService>();
            //string config = svc.GetConfig(dataId, group, 5000).Result;
            //return config;
            return GetConfigValueByApi(dataId, group);
        }

        public static string GetConfigValueByApi(string dataId, string? group = null)
        {
            if (!NacosHelper.GetNacosEnabled()) return null;//不启用Nacos

            if (group == null) group = GetNacosGroup();
            var url = GetNacosApiServer();
            url = $"{url}/nacos/v1/cs/configs?dataId={dataId}&group={group}&readTimeout=30000";
            HttpClient httpClient = new HttpClient();
            try
            {
                var responseMessage = httpClient.GetAsync(url).Result;
                var config = responseMessage.Content.ReadAsStringAsync().Result;
                if (config.IndexOf("config data not exist") > -1) config = null;
                return config;
            }
            catch (Exception ex)
            {
                throw new Exception($"从Nacos【{url}】获取配置失败：{ex.Message}");
            }
        }

        /// <summary>
        /// 获取指定的服务实例列表
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="healthy"></param>
        /// <param name="enabled"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static List<Instance> GetInstances(string serviceName, bool? healthy = true, bool? enabled = true)
        {
            if (!NacosHelper.GetNacosEnabled()) return new List<Instance>();//不启用Nacos

            if (ApplicationServices == null) throw new Exception("NacosHelper.ApplicationServices不能为空");
            INacosNamingService svc = ApplicationServices.GetService<INacosNamingService>();
            return svc.GetAllInstances(serviceName).Result
                .WhereIf(healthy != null, x => x.Healthy == healthy)
                .WhereIf(enabled != null, x => x.Enabled == enabled)
                .ToList();
        }

        public static string GetEnviromentValue(string name, string defaultValue)
        {
            return Environment.GetEnvironmentVariable(name) ?? defaultValue;
        }

        public static string GetNacosUrl()
        {
            return NacosHelper.GetEnviromentValue("NACOS_URL", "http://localhost:8848");
        }

        /// <summary>
        /// nacos命名空间
        /// </summary>
        /// <returns></returns>
        public static string GetNacosNameSpace()
        {
            return NacosHelper.GetEnviromentValue("NACOS_NAMESPACE", "5c3ffe3c-31f9-4ed4-8acf-3ee82918ee17");
        }
        /// <summary>
        /// 注册的服务名称
        /// </summary>
        /// <returns></returns>
        public static string GetNacosServiceName()
        {
            return NacosHelper.GetEnviromentValue("NACOS_SERVICENAME", "parakeet");
        }
        public static string GetNacosServiceHost()
        {
            return NacosHelper.GetEnviromentValue("NACOS_HOST", "");
        }
        /// <summary>
        /// nacos分组
        /// </summary>
        /// <returns></returns>
        public static string GetNacosGroup()
        {
            return NacosHelper.GetEnviromentValue("NACOS_GROUP", "DEFAULT_GROUP");
        }

        public static string GetNacosClusterName()
        {
            return NacosHelper.GetEnviromentValue("NACOS_ClUSTERNAME", "DEFAULT");
        }

        public static string GetPreferredNetworks()
        {
            return NacosHelper.GetEnviromentValue("NACOS_PREfERREDNETWORKS", "");
        }

        /// <summary>
        /// 权重
        /// </summary>
        /// <returns></returns>
        public static int GetAppWeight()
        {
            if (int.TryParse(NacosHelper.GetEnviromentValue("NACOS_APPWEIGHT", ""), out var res))
            {
                return res;
            }
            else
            {
                return 100;
            }
        }

        public static int GetAppPort()
        {
            if (int.TryParse(NacosHelper.GetEnviromentValue("API_PORT", ""), out var res))
            {
                return res;
            }
            else
            {
                return 0;
            }
        }
        public static bool GetUseGrpc()
        {
            if (bool.TryParse(NacosHelper.GetEnviromentValue("NACOS_USEGRPC", "true"), out var res))
            {
                return res;
            }
            else
            {
                return false;
            }
        }
        public static string GetNacosApiServer()
        {
            var url = NacosHelper.GetNacosUrl();
            if (!url.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                url = $"http://{url}";
            }
            if (url.EndsWith("/")) url = url.TrimEnd('/');
            return url;
        }

        public static bool GetNacosEnabled()
        {
            return NacosHelper.GetEnviromentValue("NACOS_ENABLE", "0") == "1";
        }
    }
}
