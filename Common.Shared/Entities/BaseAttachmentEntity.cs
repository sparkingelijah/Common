﻿using System;
using System.ComponentModel;
using Common.CustomAttributes;
using Common.ValueObjects;

namespace Common.Entities
{
    /// <summary>
    /// 附件基类
    /// </summary>
    [Description("附件基类")]
    public abstract class BaseAttachmentEntity : EntityBase
    {
        public BaseAttachmentEntity()
        {
        }

        public BaseAttachmentEntity(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 附件基本信息
        /// </summary>
        [NotSet,Description("附件基本信息")]
        public virtual Attachment Attachment { get; set; }

        /// <summary>
        /// 文件顺序使用decimal类型排序更好
        /// </summary>
        [Description("文件顺序")]
        public decimal? Order { get; set; }
    }
}
