﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 许可证
    /// </summary>

    [Table($"{CommonConsts.DefaultDbTablePrefix}{nameof(License)}", Schema = CommonConsts.PortalSchema)]
    public class License : EntityBase
    {
        public License()
        {
        }

        public License(Guid id) : base(id)
        {
        }

        /// <summary>
        /// AppId
        /// </summary>
        [Description("AppId")]
        public string AppId { get; set; }

        /// <summary>
        /// AppKey
        /// </summary>
        [Description("AppKey")]
        public string AppKey { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        [Description("AppId")]
        public string AppSecret { get; set; }

        /// <summary>
        /// Token
        /// </summary>
        [Description("Token")]
        public string Token { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        [Description("过期时间")]
        public DateTime ExpiredAt { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public string Name { get; set; }

        /// <summary>
        /// 可访问资源
        /// </summary>
        public virtual ICollection<LicenseResource> LicenseResources { get; set; } = new HashSet<LicenseResource>();
    }
}