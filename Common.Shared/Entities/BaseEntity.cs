﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.ComponentModel;
//using Volo.Abp;
//using Volo.Abp.Auditing;
//using Volo.Abp.Data;
//using Volo.Abp.Domain.Entities;
//using Volo.Abp.Domain.Entities.Auditing;
//using Volo.Abp.Uow;

//namespace Common.Entities
//{
//    /// <summary>
//    /// 实体类基类 确保只含有基础字段
//    /// </summary>
//    [Serializable, Description("实体类基类")]
//    public abstract class BaseEntity : EntityBase
//    {
//        public BaseEntity()
//        {
//        }

//        public BaseEntity(Guid id) : base(id)
//        {

//        }

//    }

//    /// <summary>
//    /// 泛型实体基类
//    /// </summary>
//    /// <typeparam name="TPrimaryKey">实体主键类型</typeparam>
//    [Serializable, Description("实体类泛型基类")]
//    public abstract class BaseEntity<TPrimaryKey> : EntityBase<TPrimaryKey>
//    {
//        protected BaseEntity()
//        {
//        }

//        public BaseEntity(TPrimaryKey id)
//            : base(id)
//        {
//        }
//    }
//}
