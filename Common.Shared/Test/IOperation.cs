﻿namespace Common.Test
{
    /// <summary>
    /// 测试Option接口
    /// </summary>
    public interface IOperation
    {
        string OperationId { get; }
    }
}
