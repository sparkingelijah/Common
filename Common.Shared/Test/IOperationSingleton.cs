﻿namespace Common.Test
{
    /// <summary>
    /// 测试OperationSingleton接口
    /// </summary>
    public interface IOperationSingleton:IOperation
    {
    }
}
