﻿using Common.Dtos;

namespace Common.Test
{
    /// <summary>
    /// 测试Option接口
    /// </summary>
    public interface ITestAutofacInterceptor
    {
        void Show(KeyValueDto input);
    }
}
