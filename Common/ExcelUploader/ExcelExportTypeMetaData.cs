﻿using System.Reflection;

namespace Common.ExcelUploader
{
    public class ExcelExportTypeMetaData
    {
        public PropertyInfo Info { get; set; }
        public int ColumnIndex { get; set; }
    }
}
