﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊障碍物
    /// </summary>
    public abstract class CraneObstacleBase : DeviceRecord
    {
        public CraneObstacleBase()
        {
        }

        public CraneObstacleBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 线性障碍物1起点x（2byte）
        /// </summary>
        [Description("线性障碍物1起点x（2byte）")]
        public virtual decimal? Linear1StartX { get; set; }

        /// <summary>
        /// 线性障碍物1起点y（2byte）
        /// </summary>
        [Description("线性障碍物1起点y（2byte）")]
        public virtual decimal? Linear1StartY { get; set; }

        /// <summary>
        /// 线性障碍物1起点z（2byte）
        /// </summary>
        [Description("线性障碍物1起点z（2byte）")]
        public virtual decimal? Linear1StartZ { get; set; }

        /// <summary>
        /// 线性障碍物1终点x（2byte）
        /// </summary>
        [Description("线性障碍物1终点x（2byte）")]
        public virtual decimal? Linear1EndX { get; set; }

        /// <summary>
        /// 线性障碍物1终点y（2byte）
        /// </summary>
        [Description("线性障碍物1终点y（2byte）")]
        public virtual decimal? Linear1EndY { get; set; }

        /// <summary>
        /// 线性障碍物1终点z（2byte）
        /// </summary>
        [Description("线性障碍物1终点z（2byte）")]
        public virtual decimal? Linear1EndZ { get; set; }

        /// <summary>
        /// 线性障碍物2起点x（2byte）
        /// </summary>
        [Description("线性障碍物2起点x（2byte）")]
        public virtual decimal? Linear2StartX { get; set; }

        /// <summary>
        /// 线性障碍物2起点y（2byte）
        /// </summary>
        [Description("线性障碍物2起点y（2byte）")]
        public virtual decimal? Linear2StartY { get; set; }

        /// <summary>
        /// 线性障碍物2起点z（2byte）
        /// </summary>
        [Description("线性障碍物2起点z（2byte）")]
        public virtual decimal? Linear2StartZ { get; set; }

        /// <summary>
        /// 线性障碍物2终点x（2byte）
        /// </summary>
        [Description("线性障碍物2终点x（2byte）")]
        public virtual decimal? Linear2EndX { get; set; }

        /// <summary>
        /// 线性障碍物2终点y（2byte）
        /// </summary>
        [Description("线性障碍物2终点y（2byte）")]
        public virtual decimal? Linear2EndY { get; set; }

        /// <summary>
        /// 线性障碍物2终点z（2byte）
        /// </summary>
        [Description("线性障碍物2终点z（2byte）")]
        public virtual decimal? Linear2EndZ { get; set; }

        /// <summary>
        /// 线性障碍物3起点x（2byte）
        /// </summary>
        [Description("线性障碍物3起点x（2byte）")]
        public virtual decimal? Linear3StartX { get; set; }

        /// <summary>
        /// 线性障碍物3起点y（2byte）
        /// </summary>
        [Description("线性障碍物3起点y（2byte）")]
        public virtual decimal? Linear3StartY { get; set; }

        /// <summary>
        /// 线性障碍物3起点z（2byte）
        /// </summary>
        [Description("线性障碍物3起点z（2byte）")]
        public virtual decimal? Linear3StartZ { get; set; }

        /// <summary>
        /// 线性障碍物3终点x（2byte）
        /// </summary>
        [Description("线性障碍物3终点x（2byte）")]
        public virtual decimal? Linear3EndX { get; set; }

        /// <summary>
        /// 线性障碍物3终点y（2byte）
        /// </summary>
        [Description("线性障碍物3终点y（2byte）")]
        public virtual decimal? Linear3EndY { get; set; }

        /// <summary>
        /// 线性障碍物3终点z（2byte）
        /// </summary>
        [Description("线性障碍物3终点z（2byte）")]
        public virtual decimal? Linear3EndZ { get; set; }

        /// <summary>
        /// 线性障碍物4起点x（2byte）
        /// </summary>
        [Description("线性障碍物4起点x（2byte）")]
        public virtual decimal? Linear4StartX { get; set; }

        /// <summary>
        /// 线性障碍物4起点y（2byte）
        /// </summary>
        [Description("线性障碍物4起点y（2byte）")]
        public virtual decimal? Linear4StartY { get; set; }

        /// <summary>
        /// 线性障碍物4起点z（2byte）
        /// </summary>
        [Description("线性障碍物4起点z（2byte）")]
        public virtual decimal? Linear4StartZ { get; set; }

        /// <summary>
        /// 线性障碍物4终点x（2byte）
        /// </summary>
        [Description("线性障碍物4终点x（2byte）")]
        public virtual decimal? Linear4EndX { get; set; }

        /// <summary>
        /// 线性障碍物4终点y（2byte）
        /// </summary>
        [Description("线性障碍物4终点y（2byte）")]
        public virtual decimal? Linear4EndY { get; set; }

        /// <summary>
        /// 线性障碍物4终点z（2byte）
        /// </summary>
        [Description("线性障碍物4终点z（2byte）")]
        public virtual decimal? Linear4EndZ { get; set; }

        /// <summary>
        /// 圆形障碍物1圆心x（2byte）
        /// </summary>
        [Description("圆形障碍物1圆心x（2byte）")]
        public virtual decimal? Circle1CenterX { get; set; }

        /// <summary>
        /// 圆形障碍物1圆心y（2byte）
        /// </summary>
        [Description("圆形障碍物1圆心y（2byte）")]
        public virtual decimal? Circle1CenterY { get; set; }

        /// <summary>
        /// 圆形障碍物1圆心z（2byte）
        /// </summary>
        [Description("圆形障碍物1圆心z（2byte）")]
        public virtual decimal? Circle1CenterZ { get; set; }

        /// <summary>
        /// 圆形障碍物1半径r（2byte）
        /// </summary>
        [Description("圆形障碍物1半径r（2byte）")]
        public virtual decimal? Circle1Radius { get; set; }

        /// <summary>
        /// 圆形障碍物2圆心x（2byte）
        /// </summary>
        [Description("圆形障碍物2圆心x（2byte）")]
        public virtual decimal? Circle2CenterX { get; set; }

        /// <summary>
        /// 圆形障碍物2圆心y（2byte）
        /// </summary>
        [Description("圆形障碍物2圆心y（2byte）")]
        public virtual decimal? Circle2CenterY { get; set; }

        /// <summary>
        /// 圆形障碍物2圆心z（2byte）
        /// </summary>
        [Description("圆形障碍物2圆心z（2byte）")]
        public virtual decimal? Circle2CenterZ { get; set; }

        /// <summary>
        /// 圆形障碍物2半径r（2byte）
        /// </summary>
        [Description("圆形障碍物2半径r（2byte）")]
        public virtual decimal? Circle2Radius { get; set; }

        /// <summary>
        /// 圆形障碍物3圆心x（2byte）
        /// </summary>
        [Description("圆形障碍物3圆心x（2byte）")]
        public virtual decimal? Circle3CenterX { get; set; }

        /// <summary>
        /// 圆形障碍物3圆心y（2byte）
        /// </summary>
        [Description("圆形障碍物3圆心y（2byte）")]
        public virtual decimal? Circle3CenterY { get; set; }

        /// <summary>
        /// 圆形障碍物3圆心z（2byte）
        /// </summary>
        [Description("圆形障碍物3圆心z（2byte）")]
        public virtual decimal? Circle3CenterZ { get; set; }

        /// <summary>
        /// 圆形障碍物3半径r（2byte）
        /// </summary>
        [Description("圆形障碍物3半径r（2byte）")]
        public virtual decimal? Circle3Radius { get; set; }

        /// <summary>
        /// 圆形障碍物4圆心x（2byte）
        /// </summary>
        [Description("圆形障碍物4圆心x（2byte）")]
        public virtual decimal? Circle4CenterX { get; set; }

        /// <summary>
        /// 圆形障碍物4圆心y（2byte）
        /// </summary>
        [Description("圆形障碍物4圆心y（2byte）")]
        public virtual decimal? Circle4CenterY { get; set; }

        /// <summary>
        /// 圆形障碍物4圆心z（2byte）
        /// </summary>
        [Description("圆形障碍物4圆心z（2byte）")]
        public virtual decimal? Circle4CenterZ { get; set; }

        /// <summary>
        /// 圆形障碍物4半径r（2byte）
        /// </summary>
        [Description("圆形障碍物4半径r（2byte）")]
        public virtual decimal? Circle4Radius { get; set; }

        /// <summary>
        /// 1号四边形障碍物A点x（2byte）
        /// </summary>
        [Description("1号四边形障碍物A点x（2byte）")]
        public virtual decimal? Quadrilateral1APointX { get; set; }

        /// <summary>
        /// 1号四边形障碍物A点y（2byte）
        /// </summary>
        [Description("1号四边形障碍物A点y（2byte）")]
        public virtual decimal? Quadrilateral1APointY { get; set; }

        /// <summary>
        /// 1号四边形障碍物A点z（2byte）
        /// </summary>
        [Description("1号四边形障碍物A点z（2byte）")]
        public virtual decimal? Quadrilateral1APointZ { get; set; }

        /// <summary>
        /// 1号四边形障碍物B点x（2byte）
        /// </summary>
        [Description("1号四边形障碍物B点x（2byte）")]
        public virtual decimal? Quadrilateral1BPointX { get; set; }

        /// <summary>
        /// 1号四边形障碍物B点y（2byte）
        /// </summary>
        [Description("1号四边形障碍物B点y（2byte）")]
        public virtual decimal? Quadrilateral1BPointY { get; set; }

        /// <summary>
        /// 1号四边形障碍物B点z（2byte）
        /// </summary>
        [Description("1号四边形障碍物B点z（2byte）")]
        public virtual decimal? Quadrilateral1BPointZ { get; set; }

        /// <summary>
        /// 1号四边形障碍物C点x（2byte）
        /// </summary>
        [Description("1号四边形障碍物C点x（2byte）")]
        public virtual decimal? Quadrilateral1CPointX { get; set; }

        /// <summary>
        /// 1号四边形障碍物C点y（2byte）
        /// </summary>
        [Description("1号四边形障碍物C点y（2byte）")]
        public virtual decimal? Quadrilateral1CPointY { get; set; }

        /// <summary>
        /// 1号四边形障碍物C点z（2byte）
        /// </summary>
        [Description("1号四边形障碍物C点z（2byte）")]
        public virtual decimal? Quadrilateral1CPointZ { get; set; }

        /// <summary>
        /// 1号四边形障碍物D点x（2byte）
        /// </summary>
        [Description("1号四边形障碍物D点x（2byte）")]
        public virtual decimal? Quadrilateral1DPointX { get; set; }

        /// <summary>
        /// 1号四边形障碍物D点y（2byte）
        /// </summary>
        [Description("1号四边形障碍物D点y（2byte）")]
        public virtual decimal? Quadrilateral1DPointY { get; set; }

        /// <summary>
        /// 1号四边形障碍物D点z（2byte）
        /// </summary>
        [Description("1号四边形障碍物D点z（2byte）")]
        public virtual decimal? Quadrilateral1DPointZ { get; set; }

        /// <summary>
        /// 2号四边形障碍物A点x（2byte）
        /// </summary>
        [Description("2号四边形障碍物A点x（2byte）")]
        public virtual decimal? Quadrilateral2APointX { get; set; }

        /// <summary>
        /// 2号四边形障碍物A点y（2byte）
        /// </summary>
        [Description(" 2号四边形障碍物A点y（2byte）")]
        public virtual decimal? Quadrilateral2APointY { get; set; }

        /// <summary>
        /// 2号四边形障碍物A点z（2byte）
        /// </summary>
        [Description("2号四边形障碍物A点z（2byte）")]
        public virtual decimal? Quadrilateral2APointZ { get; set; }

        /// <summary>
        /// 2号四边形障碍物B点x（2byte）
        /// </summary>
        [Description("2号四边形障碍物B点x（2byte）")]
        public virtual decimal? Quadrilateral2BPointX { get; set; }

        /// <summary>
        /// 2号四边形障碍物B点y（2byte）
        /// </summary>
        [Description("2号四边形障碍物B点y（2byte）")]
        public virtual decimal? Quadrilateral2BPointY { get; set; }

        /// <summary>
        /// 2号四边形障碍物B点z（2byte）
        /// </summary>
        [Description("2号四边形障碍物B点z（2byte）")]
        public virtual decimal? Quadrilateral2BPointZ { get; set; }

        /// <summary>
        /// 2号四边形障碍物C点x（2byte）
        /// </summary>
        [Description("2号四边形障碍物C点x（2byte）")]
        public virtual decimal? Quadrilateral2CPointX { get; set; }

        /// <summary>
        /// 2号四边形障碍物C点y（2byte）
        /// </summary>
        [Description("2号四边形障碍物C点y（2byte）")]
        public virtual decimal? Quadrilateral2CPointY { get; set; }

        /// <summary>
        /// 2号四边形障碍物C点z（2byte）
        /// </summary>
        [Description("2号四边形障碍物C点z（2byte）")]
        public virtual decimal? Quadrilateral2CPointZ { get; set; }

        /// <summary>
        /// 2号四边形障碍物D点x（2byte）
        /// </summary>
        [Description("2号四边形障碍物D点x（2byte）")]
        public virtual decimal? Quadrilateral2DPointX { get; set; }

        /// <summary>
        /// 2号四边形障碍物D点y（2byte）
        /// </summary>
        [Description("2号四边形障碍物D点y（2byte）")]
        public virtual decimal? Quadrilateral2DPointY { get; set; }

        /// <summary>
        /// 2号四边形障碍物D点z（2byte）
        /// </summary>
        [Description("2号四边形障碍物D点z（2byte）")]
        public virtual decimal? Quadrilateral2DPointZ { get; set; }

        /// <summary>
        /// 3号四边形障碍物A点x（2byte）
        /// </summary>
        [Description(" 3号四边形障碍物A点x（2byte）")]
        public virtual decimal? Quadrilateral3APointX { get; set; }

        /// <summary>
        /// 3号四边形障碍物A点y（2byte）
        /// </summary>
        [Description(" 3号四边形障碍物A点y（2byte）")]
        public virtual decimal? Quadrilateral3APointY { get; set; }

        /// <summary>
        /// 3号四边形障碍物A点z（2byte）
        /// </summary>
        [Description("3号四边形障碍物A点z（2byte）")]
        public virtual decimal? Quadrilateral3APointZ { get; set; }

        /// <summary>
        /// 3号四边形障碍物B点x（2byte）
        /// </summary>
        [Description("3号四边形障碍物B点x（2byte）")]
        public virtual decimal? Quadrilateral3BPointX { get; set; }

        /// <summary>
        /// 3号四边形障碍物B点y（2byte）
        /// </summary>
        [Description("3号四边形障碍物B点y（2byte）")]
        public virtual decimal? Quadrilateral3BPointY { get; set; }

        /// <summary>
        /// 3号四边形障碍物B点z（2byte）
        /// </summary>
        [Description("3号四边形障碍物B点z（2byte）")]
        public virtual decimal? Quadrilateral3BPointZ { get; set; }

        /// <summary>
        /// 3号四边形障碍物C点x（2byte）
        /// </summary>
        [Description("3号四边形障碍物C点x（2byte）")]
        public virtual decimal? Quadrilateral3CPointX { get; set; }

        /// <summary>
        /// 3号四边形障碍物C点y（2byte）
        /// </summary>
        [Description(" 3号四边形障碍物C点y（2byte）")]
        public virtual decimal? Quadrilateral3CPointY { get; set; }

        /// <summary>
        /// 3号四边形障碍物C点z（2byte）
        /// </summary>
        [Description("3号四边形障碍物C点z（2byte）")]
        public virtual decimal? Quadrilateral3CPointZ { get; set; }

        /// <summary>
        /// 3号四边形障碍物D点x（2byte）
        /// </summary>
        [Description("3号四边形障碍物D点x（2byte）")]
        public virtual decimal? Quadrilateral3DPointX { get; set; }

        /// <summary>
        /// 3号四边形障碍物D点y（2byte）
        /// </summary>
        [Description("3号四边形障碍物D点y（2byte）")]
        public virtual decimal? Quadrilateral3DPointY { get; set; }

        /// <summary>
        /// 3号四边形障碍物D点z（2byte）
        /// </summary>
        [Description("3号四边形障碍物D点z（2byte）")]
        public virtual decimal? Quadrilateral3DPointZ { get; set; }

        /// <summary>
        /// 4号四边形障碍物A点x（2byte）
        /// </summary>
        [Description(" 4号四边形障碍物A点x（2byte）")]
        public virtual decimal? Quadrilateral4APointX { get; set; }

        /// <summary>
        /// 4号四边形障碍物A点y（2byte）
        /// </summary>
        [Description("4号四边形障碍物A点y（2byte）")]
        public virtual decimal? Quadrilateral4APointY { get; set; }

        /// <summary>
        /// 4号四边形障碍物A点z（2byte）
        /// </summary>
        [Description("4号四边形障碍物A点z（2byte）")]
        public virtual decimal? Quadrilateral4APointZ { get; set; }

        /// <summary>
        /// 4号四边形障碍物B点x（2byte）
        /// </summary>
        [Description("4号四边形障碍物B点x（2byte）")]
        public virtual decimal? Quadrilateral4BPointX { get; set; }

        /// <summary>
        /// 4号四边形障碍物B点y（2byte）
        /// </summary>
        [Description("4号四边形障碍物B点y（2byte）")]
        public virtual decimal? Quadrilateral4BPointY { get; set; }

        /// <summary>
        /// 4号四边形障碍物B点z（2byte）
        /// </summary>
        [Description("4号四边形障碍物B点z（2byte）")]
        public virtual decimal? Quadrilateral4BPointZ { get; set; }

        /// <summary>
        /// 4号四边形障碍物C点x（2byte）
        /// </summary>
        [Description("4号四边形障碍物C点x（2byte）")]
        public virtual decimal? Quadrilateral4CPointX { get; set; }

        /// <summary>
        /// 4号四边形障碍物C点y（2byte）
        /// </summary>
        [Description("4号四边形障碍物C点y（2byte）")]
        public virtual decimal? Quadrilateral4CPointY { get; set; }

        /// <summary>
        /// 4号四边形障碍物C点z（2byte）
        /// </summary>
        [Description("4号四边形障碍物C点z（2byte）")]
        public virtual decimal? Quadrilateral4CPointZ { get; set; }

        /// <summary>
        /// 4号四边形障碍物D点x（2byte）
        /// </summary>
        [Description("4号四边形障碍物D点x（2byte）")]
        public virtual decimal? Quadrilateral4DPointX { get; set; }

        /// <summary>
        /// 4号四边形障碍物D点y（2byte）
        /// </summary>
        [Description("4号四边形障碍物D点y（2byte）")]
        public virtual decimal? Quadrilateral4DPointY { get; set; }

        /// <summary>
        /// 4号四边形障碍物D点z（2byte）
        /// </summary>
        [Description("4号四边形障碍物D点z（2byte）")]
        public virtual decimal? Quadrilateral4DPointZ { get; set; }
    }
}