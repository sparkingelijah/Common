﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊抓拍
    /// </summary>
    public abstract class CraneCabPhotoBase : DeviceRecord
    {
        public CraneCabPhotoBase()
        {
        }

        public CraneCabPhotoBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 抓拍图片
        /// </summary>
        [Description("抓拍图片")]
        public virtual string Photo { get; set; }

        /// <summary>
        /// 视频设备编号
        /// </summary>
        [MaxLength(50), Description("视频设备编号")]
        public virtual string CameraNo { get; set; }

        /// <summary>
        /// 设备抓拍的时间
        /// </summary>
        [Description("设备抓拍的时间")]
        public virtual DateTime SnapshotTime { get; set; }
    }
}