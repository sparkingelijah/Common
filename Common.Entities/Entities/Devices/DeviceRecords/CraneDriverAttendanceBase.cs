﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊驾驶员考勤
    /// </summary>
    public abstract class CraneDriverAttendanceBase : DeviceRecord
    {
        public CraneDriverAttendanceBase()
        {
        }

        public CraneDriverAttendanceBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔吊编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 身份证
        /// </summary>
        [MaxLength(18)]
        [Description("身份证")]
        public virtual string IdCard { get; set; }

        /// <summary>
        /// 驾驶员名字
        /// </summary>
        [MaxLength(20)]
        [Description("驾驶员名字")]
        public virtual string DriverName { get; set; }

        /// <summary>
        /// 门禁考勤图片的数据
        /// </summary>
        [Description("门禁考勤图片的数据")]
        public virtual string Photo { get; set; }
    }
}