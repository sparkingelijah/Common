﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊报警
    /// </summary>
    public abstract class CraneAlarmBase : DeviceRecord
    {
        public CraneAlarmBase()
        {
        }

        public CraneAlarmBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 吊绳倍率
        /// </summary>
        [Description("吊绳倍率")]
        public virtual int? Fall { get; set; }

        /// <summary>
        /// 高度数据(2byte) 吊钩到地面的高度
        /// 高度实际值1代表0.1米，幅度实际值1代表0.1米，回转1代表0.1度。
        /// 重量1代表1kg，力矩值1代表1个百分比，风速值1代表0.1米/秒，倾斜值1米/秒。
        /// </summary>
        [Description("高度数据(2byte) 吊钩到地面的高度 高度实际值1代表0.1米，幅度实际值1代表0.1米，回转1代表0.1度 重量1代表1kg，力矩值1代表1个百分比，风速值1代表0.1米/秒，倾斜值1米/秒 ")]
        public virtual decimal? Height { get; set; }

        /// <summary>
        /// 幅度数据(2byte)
        /// </summary>
        [Description("幅度数据(2byte)")]
        public virtual decimal? Range { get; set; }

        /// <summary>
        /// 回转(2byte) 以正北为0度 顺时针
        /// </summary>
        [Description("回转(2byte) 以正北为0度 顺时针")]
        public virtual decimal? Rotation { get; set; }

        /// <summary>
        /// 起重量数据(2byte)
        /// </summary>
        [Description("起重量数据(2byte)")]
        public virtual decimal? Load { get; set; }

        /// <summary>
        /// 重量百分比（1byte）
        /// </summary>
        [Description("重量百分比（1byte）")]
        public virtual decimal? WeightPercent { get; set; }

        /// <summary>
        /// 风速数据(2byte)
        /// </summary>
        [Description("风速数据(2byte)")]
        public virtual decimal? WindSpeed { get; set; }

        /// <summary>
        /// 倾角数据(2byte) tilt angle:倾斜角度
        /// </summary>
        [Description("倾角数据(2byte) tilt angle:倾斜角度")]
        public virtual decimal? TiltAngle { get; set; }

        /// <summary>
        /// 当前力矩
        /// </summary>
        [Description("当前力矩")]
        public virtual decimal? Torque { get; set; }

        /// <summary>
        /// 力矩百分比（1byte）
        /// </summary>
        [Description("力矩百分比（1byte）")]
        public virtual decimal? TorquePercent { get; set; }
    }
}