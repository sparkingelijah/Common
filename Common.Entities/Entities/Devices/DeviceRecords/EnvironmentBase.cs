﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 环境监测
    /// </summary>
    public abstract class EnvironmentBase : DeviceRecord
    {
        public EnvironmentBase()
        {
        }

        public EnvironmentBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// PM2.5
        /// </summary>
        [Description("PM2.5")]
        public virtual decimal? PM2P5 { get; set; }

        /// <summary>
        /// PM10
        /// </summary>
        [Description("PM10")]
        public virtual decimal? PM10 { get; set; }

        /// <summary>
        /// 温度
        /// </summary>
        [Description("温度")]
        public virtual decimal? Temperature { get; set; }

        /// <summary>
        /// 噪声
        /// </summary>
        [Description("噪声")]
        public virtual decimal? Noise { get; set; }

        /// <summary>
        /// 湿度
        /// </summary>
        [Description("湿度")]
        public virtual decimal? Humidity { get; set; }

        /// <summary>
        /// 风向
        /// </summary>
        [Description("风向")]
        public virtual decimal? WindDirection { get; set; }

        /// <summary>
        /// 风速
        /// </summary>
        [Description("风速")]
        public virtual decimal? WindSpeed { get; set; }

        /// <summary>
        /// 雨量
        /// </summary>
        [Description("雨量")]
        public virtual decimal? Rainfall { get; set; }

        /// <summary>
        /// 气压
        /// </summary>
        [Description("气压")]
        public virtual decimal? Pressure { get; set; }

        /// <summary>
        /// 一氧化碳
        /// </summary>
        [Description("一氧化碳")]
        public virtual decimal? CO { get; set; }

        /// <summary>
        /// 二氧化硫
        /// </summary>
        [Description("二氧化硫")]
        public virtual decimal? SO2 { get; set; }

        /// <summary>
        /// 二氧化氮
        /// </summary>
        [Description("二氧化氮")]
        public virtual decimal? NO2 { get; set; }

        /// <summary>
        /// 臭氧
        /// </summary>
        [Description("臭氧")]
        public virtual decimal? O3 { get; set; }

        /// <summary>
        /// 能见度
        /// </summary>
        [Description("能见度")]
        public virtual decimal? Visibility { get; set; }

        /// <summary>
        /// 总挥发性有机化合物
        /// 苯类、烷类、芳烃类、烯类、卤烃类、酯类、醛类、酮类和其他。
        /// </summary>
        [Description("总挥发性有机化合物(苯类、烷类、芳烃类、烯类、卤烃类、酯类、醛类、酮类和其他)")]
        public virtual decimal? TVOC { get; set; }

        /// <summary>
        /// 空气质量指数
        /// </summary>
        [Description("空气质量指数")]
        public virtual decimal? AQI { get; set; }

        /// <summary>
        /// 总悬浮颗粒物
        /// </summary>
        [Description("总悬浮颗粒物")]
        public virtual decimal? TSP { get; set; }
    }
}
