﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊报警设置
    /// </summary>
    public abstract class CraneAlarmSettingBase : DeviceRecord
    {
        public CraneAlarmSettingBase()
        {
        }

        public CraneAlarmSettingBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 水平距离报警值（2byte）
        /// </summary>
        [Description("水平距离报警值（2byte）")]
        public virtual decimal? AlarmHorizontalDistance { get; set; }

        /// <summary>
        /// 垂直距离报警值（2byte）
        /// </summary>
        [Description(" 垂直距离报警值（2byte）")]
        public virtual decimal? AlarmVerticalDistance { get; set; }

        /// <summary>
        /// 重量报警值（2byte）
        /// </summary>
        [Description("重量报警值（2byte）")]
        public virtual decimal? AlarmWeight { get; set; }

        /// <summary>
        /// 力矩报警值（2byte）
        /// </summary>
        [Description("力矩报警值（2byte）")]
        public virtual decimal? AlarmTorque { get; set; }

        /// <summary>
        /// 风速报警值（2byte）
        /// </summary>
        [Description("风速报警值（2byte）")]
        public virtual decimal? AlarmWindSpeed { get; set; }

        /// <summary>
        /// 倾斜报警值（2byte）
        /// </summary>
        [Description("倾斜报警值（2byte）")]
        public virtual decimal? AlarmTilt { get; set; }

        /// <summary>
        /// 水平距离预警值（2byte）
        /// </summary>
        [Description("水平距离预警值（2byte）")]
        public virtual decimal? WarningHorizontalDistance { get; set; }

        /// <summary>
        ///  垂直距离预警值（2byte）
        /// </summary>
        [Description("垂直距离预警值（2byte）")]
        public virtual decimal? WarningVerticalDistance { get; set; }

        /// <summary>
        /// 重量预警值（2byte）
        /// </summary>
        [Description("重量预警值（2byte）")]
        public virtual decimal? WarningWeight { get; set; }

        /// <summary>
        /// 力矩预警值（2byte）
        /// </summary>
        [Description("力矩预警值（2byte）")]
        public virtual decimal? WarningTorque { get; set; }

        /// <summary>
        /// 风速预警值（2byte）
        /// </summary>
        [Description("风速预警值（2byte）")]
        public virtual decimal? WarningWindSpeed { get; set; }

        /// <summary>
        /// 倾斜预警值（2byte）
        /// </summary>
        [Description("倾斜预警值（2byte）")]
        public virtual decimal? WarningTilt { get; set; }
    }
}