﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊保护区
    /// </summary>
    public abstract class CraneReserveBase : DeviceRecord
    {
        public CraneReserveBase()
        {
        }

        public CraneReserveBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 保护区类型
        /// </summary>
        [Description("保护区类型")]
        public virtual int? Type { get; set; }

        /// <summary>
        /// 保护区类型
        /// </summary>
        [MaxLength(50), Description("保护区类型")]
        public virtual string Name { get; set; }

        /// <summary>
        /// 保护区的id号 范围：0-65536
        /// </summary>
        [Description("保护区的id号 范围：0-65536")]
        public virtual int ZoneId { get; set; }

        /// <summary>
        ///int
        ///0其他
        ///1医院
        ///2学校
        ///3广场
        ///4道路
        ///5居民区
        ///6办公区
        ///7高压线；其余类型待定（保留，只对禁行区有效）
        /// </summary>
        [Description("int 0其他 1医院 2学校 3广场 4道路 5居民区 6办公区 7高压线；其余类型待定（保留，只对禁行区有效）")]
        public virtual int BuildingType { get; set; }

        /// <summary>
        /// 保护区平面左上角相对于塔机中心的X坐标
        /// </summary>
        [Description("保护区平面左上角相对于塔机中心的X坐标")]
        public virtual decimal? LocationX { get; set; }

        /// <summary>
        /// 保护区平面左上角相对于塔机中心的Y坐标
        /// </summary>
        [Description("保护区平面左上角相对于塔机中心的Y坐标")]
        public virtual decimal? LocationY { get; set; }

        /// <summary>
        /// 保护区平面宽度
        /// </summary>
        [Description("保护区平面宽度")]
        public virtual decimal? Width { get; set; }

        /// <summary>
        /// 保护区平面高度
        /// </summary>
        [Description("保护区平面高度")]
        public virtual decimal? Height { get; set; }
    }
}