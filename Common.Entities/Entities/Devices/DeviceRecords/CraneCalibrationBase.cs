﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊定位
    /// </summary>
    public abstract class CraneCalibrationBase : DeviceRecord
    {
        public CraneCalibrationBase()
        {
        }

        public CraneCalibrationBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 近端幅度标定AD值（2字节）
        /// </summary>
        [Description("近端幅度标定AD值（2字节）")]
        public virtual decimal? ProximalRangeCalibrationAD { get; set; }

        /// <summary>
        /// 近端幅度标定实际值（2字节）
        /// </summary>
        [Description("近端幅度标定实际值（2字节）")]
        public virtual decimal? ProximalRangeCalibrationActual { get; set; }

        /// <summary>
        /// 远端幅度标定AD值（2字节）
        /// </summary>
        [Description("远端幅度标定AD值（2字节）")]
        public virtual decimal? RemoteRangeCalibrationAD { get; set; }

        /// <summary>
        /// 远端幅度标定实际值（2字节）
        /// </summary>
        [Description("远端幅度标定实际值（2字节）")]
        public virtual decimal? RemoteRangeCalibrationActual { get; set; }

        /// <summary>
        /// 近端高度标定AD值（2byte）
        /// </summary>
        [Description("近端高度标定AD值（2byte）")]
        public virtual decimal? ProximalHeightCalibrationAD { get; set; }

        /// <summary>
        /// 高度近端标定实际值（2byte）
        /// </summary>
        [Description("高度近端标定实际值（2byte）")]
        public virtual decimal? ProximalHeightCalibrationActual { get; set; }

        /// <summary>
        /// 远端高度标定AD值（2byte）
        /// </summary>
        [Description("远端高度标定AD值（2byte）")]
        public virtual decimal? RemoteHeightCalibrationAD { get; set; }

        /// <summary>
        /// 高度远端标定实际值（2byte）
        /// </summary>
        [Description("高度远端标定实际值（2byte）")]
        public virtual decimal? RemoteHeightCalibrationActual { get; set; }

        /// <summary>
        /// 空载重量AD值（2byte）
        /// </summary>
        [Description("空载重量AD值（2byte）")]
        public virtual decimal? EmptyLoadWeightAD { get; set; }

        /// <summary>
        /// 空载重量实际值（2byte）
        /// </summary>
        [Description("空载重量实际值（2byte）")]
        public virtual decimal? EmptyLoadWeightActual { get; set; }

        /// <summary>
        /// 负载重量AD值（2byte）
        /// </summary>
        [Description("负载重量AD值（2byte）")]
        public virtual decimal? LoadWeightAD { get; set; }

        /// <summary>
        /// 负载重量实际值（2byte）
        /// </summary>
        [Description("负载重量实际值（2byte）")]
        public virtual decimal? LoadWeightActual { get; set; }

        /// <summary>
        /// 回转起点AD值（2byte）
        /// </summary>
        [Description("回转起点AD值（2byte）")]
        public virtual decimal? RotationStartingPointAD { get; set; }

        /// <summary>
        /// 回转起点实际角度（2byte）
        /// </summary>
        [Description("回转起点实际角度（2byte）")]
        public virtual decimal? RotationStartingPointActual { get; set; }

        /// <summary>
        /// 回转终点AD值（2byte）
        /// </summary>
        [Description("回转终点AD值（2byte）")]
        public virtual decimal? RotationEndinigPointAD { get; set; }

        /// <summary>
        /// 回转终点实际角度（2byte）
        /// </summary>
        [Description("回转终点实际角度（2byte）")]
        public virtual decimal? RotationEndinigPointActual { get; set; }

        /// <summary>
        /// 风速校准系数（2byte）
        /// </summary>
        [Description("风速校准系数（2byte）")]
        public virtual decimal? WindSpeedCalibration { get; set; }

        /// <summary>
        /// 预留（6byte）
        /// </summary>
        [Description("预留（6byte）")]
        public virtual short Reserved { get; set; }

        /// <summary>
        /// 倾斜校准系数（2byte）
        /// </summary>
        [Description("倾斜校准系数（2byte）")]
        public virtual decimal? TiltCalibration { get; set; }
    }
}