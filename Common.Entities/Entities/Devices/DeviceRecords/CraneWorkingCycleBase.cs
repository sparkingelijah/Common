﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 吊装数据
    /// </summary>
    public abstract class CraneWorkingCycleBase : DeviceRecord
    {
        public CraneWorkingCycleBase()
        {
        }

        public CraneWorkingCycleBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Required]
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 开始时间（6byte年月日时分秒)
        /// </summary>
        [Description("开始时间（6byte年月日时分秒)")]
        public virtual DateTime StartTime { get; set; }

        /// <summary>
        /// 结束时间（6byte年月日时分秒)
        /// </summary>
        [Description("结束时间（6byte年月日时分秒)")]
        public virtual DateTime EndTime { get; set; }

        /// <summary>
        /// 本次循环最大吊重（2byte）无负值
        /// </summary>
        [Description("本次循环最大吊重（2byte）无负值")]
        public virtual decimal? MaxWeight { get; set; }

        /// <summary>
        /// 本次循环最大力矩（2byte）无负值
        /// </summary>
        [Description("本次循环最大力矩（2byte）无负值")]
        public virtual decimal? MaxTorque { get; set; }

        /// <summary>
        /// 最大高度（2byte）
        /// </summary>
        [Description("最大高度（2byte）")]
        public virtual decimal? MaxHeight { get; set; }

        /// <summary>
        /// 最小高度（2byte）
        /// </summary>
        [Description("最小高度（2byte）")]
        public virtual decimal? MinHeight { get; set; }

        /// <summary>
        /// 最大幅度（2byte）无负值
        /// </summary>
        [Description("最大幅度（2byte）无负值")]
        public virtual decimal? MaxRange { get; set; }

        /// <summary>
        /// 最小幅度（2byte）无负值
        /// </summary>
        [Description("最小幅度（2byte）无负值")]
        public virtual decimal? MinRange { get; set; }

        /// <summary>
        /// 起吊点角度（2byte）
        /// </summary>
        [Description("起吊点角度（2byte）")]
        public virtual decimal? LoadPointAngle { get; set; }

        /// <summary>
        /// 起吊点幅度（2byte）无负值
        /// </summary>
        [Description("起吊点幅度（2byte）无负值")]
        public virtual decimal? LoadPointRange { get; set; }

        /// <summary>
        /// 起吊点高度(2byte)
        /// </summary>
        [Description("起吊点高度(2byte)")]
        public virtual decimal? LoadPointHeight { get; set; }

        /// <summary>
        /// 卸吊点角度（2byte）
        /// </summary>
        [Description("卸吊点角度（2byte）")]
        public virtual decimal? UnloadPointAngle { get; set; }

        /// <summary>
        /// 卸吊点幅度（2byte）无负值
        /// </summary>
        [Description("卸吊点幅度（2byte）无负值")]
        public virtual decimal? UnloadPointRange { get; set; }

        /// <summary>
        /// 卸吊点高度（2byte）
        /// </summary>
        [Description("卸吊点高度（2byte）")]
        public virtual decimal? UnloadPointHeight { get; set; }
    }
}