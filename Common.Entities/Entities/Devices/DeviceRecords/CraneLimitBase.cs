﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊限位
    /// </summary>
    public abstract class CraneLimitBase : DeviceRecord
    {
        public CraneLimitBase()
        {
        }

        public CraneLimitBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 幅度限位起点值（2byte）
        /// </summary>
        [Description("幅度限位起点值（2byte）")]
        public virtual decimal? RangeLimitStarting { get; set; }

        /// <summary>
        /// 幅度限位终点值（2byte）
        /// </summary>
        [Description("幅度限位终点值（2byte）")]
        public virtual decimal? RangeLimitEnding { get; set; }

        /// <summary>
        /// 高度限位起点值（2byte）
        /// </summary>
        [Description("高度限位起点值（2byte）")]
        public virtual decimal? HeightLimitStarting { get; set; }

        /// <summary>
        /// 高度限位终点值（2byte）
        /// </summary>
        [Description("高度限位终点值（2byte）")]
        public virtual decimal? HeightLimitEnding { get; set; }

        /// <summary>
        /// 回转限位起点值（2byte）
        /// </summary>
        [Description("回转限位起点值（2byte）")]
        public virtual decimal? RotationLimitStarting { get; set; }

        /// <summary>
        /// 回转限位终点值（2byte）
        /// </summary>
        [Description("回转限位终点值（2byte）")]
        public virtual decimal? RotationLimitEnding { get; set; }
    }
}