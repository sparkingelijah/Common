﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Common.Enums;

namespace Common.Entities
{
    /// <summary>
    /// 考勤
    /// </summary>
    [Description("考勤")]
    public abstract class GateBase : DeviceRecordBase
    {
        public GateBase()
        {
        }

        public GateBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 人员唯一标识
        /// </summary>
        [Required]
        [MaxLength(50)]
        [Description("人员唯一标识")]
        public virtual string PersonnelId { get; set; }

        /// <summary>
        /// 员工名字
        /// </summary>
        [MaxLength(50)]
        [Description("员工名字")]
        public virtual string PersonnelName { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        [MaxLength(18)]
        [Description("身份证号")]
        public virtual string IdCard { get; set; }

        /// <summary>
        /// 进出状态 【1-进】 【2-出】 【3-采集】
        /// </summary>
        [Description("进出状态 【1-进】 【2-出】 【3-采集】")]
        public virtual EntryState InOrOut { get; set; } = EntryState.进场;

        /// <summary>
        /// 考勤照片
        /// </summary>
        [MaxLength(CommonConsts.MaxLength8192)]
        [Description("考勤照片")]
        public virtual string Photo { get; set; }

        /// <summary>
        /// 考勤照片Url
        /// </summary>
        [MaxLength(CommonConsts.MaxLength512)]
        [Description("考勤照片Url")]
        public virtual string PhotoUrl { get; set; }

        /// <summary>
        /// 用户工号（建委下发的）
        /// </summary>
        [MaxLength(CommonConsts.MaxLength64)]
        [Description("用户工号")]
        public virtual string WorkerNo { get; set; }

    }
}
