﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊故障
    /// </summary>
    public abstract class CraneFaultBase : DeviceRecord
    {
        public CraneFaultBase()
        {
        }

        public CraneFaultBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号
        /// </summary>
        [Description("塔机在当前塔群中的编号")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 事件类型
        /// 1：报警 2：故障 3：机械限位
        /// </summary
        [Description("事件类型  1：报警 2：故障 3：机械限位")]
        public virtual int EventType { get; set; }

        /// <summary>
        ///事件信息的数据
        ///int
        ///0:正常
        ///    其他为故障
        ///Bit[0]:变幅传感器故障
        ///    Bit[1]:高度传感器故障
        ///    Bit[2]:转角传感器故障
        ///    Bit[3]:吊重传感器故障
        ///    Bit[4]:风速传感器故障
        ///    Bit[5]:海拔传感器故障（暂不用）
        ///Bit[6]:倾角传感器故障（暂不用）
        ///Bit[7]:语音电路故障
        ///    Bit[8]:无线模块故障
        ///    Bit[9]:存储系统故障
        ///    Bit[10]:主供电掉电
        ///    Bit[11]: 参数设置故障
        ///    Bit[12]: 时钟故障
        ///    Bit[13]: 3G模块故障
        ///    Bit[14]: 显示屏通讯故障
        ///    Bit[15]：保留
        /// </summary>
        [Description("事件信息的数据")]
        public virtual int EventMessage { get; set; }
    }
}