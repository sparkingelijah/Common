﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊基础数据
    /// </summary>
    public abstract class CraneBasicBase : DeviceRecord
    {
        public CraneBasicBase()
        {
        }

        public CraneBasicBase(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 塔机在当前塔群中的编号 塔吊唯一标识(UniqueId)
        /// </summary>
        [Description("塔机在当前塔群中的编号 塔吊唯一标识(UniqueId)")]
        public virtual short CraneId { get; set; }

        /// <summary>
        /// 小臂长
        /// </summary>
        [Description("小臂长")]
        public virtual decimal? ShortArm { get; set; }

        /// <summary>
        /// 大臂长
        /// </summary>
        [Description("大臂长")]
        public virtual decimal? LongArm { get; set; }

        /// <summary>
        /// 塔帽高/塔高 单位0.1m 2byte
        /// </summary>
        [Description("塔帽高/塔高 单位0.1m 2byte")]
        public virtual decimal? TowerHatHeight { get; set; }

        /// <summary>
        /// 起重臂高(塔臂高) 单位0.1m 2byte
        /// </summary>
        [Description("起重臂高(塔臂高) 单位0.1m 2byte")]
        public virtual decimal? BoomHeight { get; set; }

        /// <summary>
        /// 最大吊重 2byte
        /// </summary>
        [Description("最大吊重 2byte")]
        public virtual decimal? MaxLoadWeight { get; set; }

        /// <summary>
        /// 最大力矩 2byte
        /// </summary>
        [Description("最大力矩 2byte")]
        public virtual decimal? MaxTorque { get; set; }

        /// <summary>
        /// 吊绳倍率
        /// </summary>
        [Description("吊绳倍率")]
        public virtual int? Fall { get; set; }

        /// <summary>
        /// 吊钩重量 2byte
        /// </summary>
        [Description("吊钩重量 2byte")]
        public virtual decimal? HookWeight { get; set; }

        /// <summary>
        /// 塔机转角为0时与正北夹角，以右为正.范围：0-360
        /// </summary>
        [Description("塔机转角为0时与正北夹角，以右为正.范围：0-360")]
        public virtual decimal? CompassAngle { get; set; }

        /// <summary>
        /// 坐标x 单位0.1m 2byte
        /// </summary>
        [Description("坐标x 单位0.1m 2byte")]
        public virtual decimal? X { get; set; }

        /// <summary>
        /// 坐标y 单位0.1m 2byte
        /// </summary>
        [Description("坐标y 单位0.1m 2byte")]
        public virtual decimal? Y { get; set; }
    }
}