﻿using Common.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 环境监测
    /// </summary>
    [Table($"{CommonConsts.DevicePrefix}{nameof(EnvironmentRecord)}", Schema = CommonConsts.DeviceSchema)]
    public class EnvironmentRecord : EnvironmentBase, IBatchInsertSupport
    {
        public EnvironmentRecord()
        {
        }

        public EnvironmentRecord(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 一氧化碳标识
        /// </summary>
        public virtual string COFlag { get; set; }

        /// <summary>
        /// 二氧化硫标识
        /// </summary>
        public virtual string SO2Flag { get; set; }

        /// <summary>
        /// 二氧化氮标识
        /// </summary>
        public virtual string NO2Flag { get; set; }

        /// <summary>
        /// 臭氧标识
        /// </summary>
        public virtual string O3Flag { get; set; }

        /// <summary>
        /// 能见度标识
        /// </summary>
        public virtual string VisibilityFlag { get; set; }

        /// <summary>
        /// 总挥发性有机化合物标识
        /// </summary>
        public virtual string TVOCFlag { get; set; }

        /// <summary>
        /// 总悬浮颗粒物设备标志
        /// </summary>
        public virtual string TSPFlag { get; set; }

        /// <summary>
        /// PM2.5设备标志
        /// </summary>
        public virtual string PM2P5Flag { get; set; }

        /// <summary>
        /// PM10设备标志
        /// </summary>
        public virtual string PM10Flag { get; set; }

        /// <summary>
        /// 噪声设备标志
        /// </summary>
        public virtual string NoiseFlag { get; set; }

        /// <summary>
        /// 风向设备标志
        /// </summary>
        public virtual string WindDirectionFlag { get; set; }

        /// <summary>
        /// 风速设备标志
        /// </summary>
        public virtual string WindSpeedFlag { get; set; }

        /// <summary>
        /// 温度设备标志
        /// </summary>
        public virtual string TemperatureFlag { get; set; }

        /// <summary>
        /// 湿度设备标志
        /// </summary>
        public virtual string HumidityFlag { get; set; }

        /// <summary>
        /// 气压设备标志
        /// </summary>
        public virtual string PressureFlag { get; set; }

        /// <summary>
        /// 雨量设备标志
        /// </summary>
        public virtual string RainfallFlag { get; set; }
    }
}