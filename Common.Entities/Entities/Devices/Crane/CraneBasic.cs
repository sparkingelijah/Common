
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔机基础信息 /api/dev/tower/upload/tw-basic-info
    /// </summary>
    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneBasic)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneBasic : CraneBasicBase
    {
        public CraneBasic()
        {
        }

        public CraneBasic(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 版本号
        /// </summary>
        /// <value></value>
        [MaxLength(10)]
        public virtual string Version { get; set; }

        /// <summary>
        /// 塔机型号 30byte
        /// </summary>
        [MaxLength(20)]
        public virtual string CraneType { get; set; }

        /// <summary>
        /// 铰接长度 2byte
        /// </summary>
        public virtual decimal? ArticulatedLength { get; set; }

        #region 补充

        /// <summary>
        /// 塔吊名字
        /// </summary>
        public virtual string CraneName { get; set; }

        /// <summary>
        /// 载重最小值
        /// </summary>
        public virtual decimal? MinLoadWeight { get; set; }

        /// <summary>
        /// 力矩最小值
        /// </summary>
        public virtual decimal? MinTorque { get; set; }

        /// <summary>
        /// 倾角最小值
        /// </summary>
        public virtual decimal? MinAngle { get; set; }

        /// <summary>
        /// 倾角最大值
        /// </summary>
        public virtual decimal? MaxAngle { get; set; }

        /// <summary>
        /// 工作幅度最小值
        /// </summary>
        /// <value></value>
        public virtual decimal? MinRadius { get; set; }

        /// <summary>
        /// 工作幅度最大值
        /// </summary>
        /// <value></value>
        public virtual decimal? MaxRadius { get; set; }

        /// <summary>
        /// 工作高度最小值
        /// </summary>
        /// <value></value>
        public virtual decimal? MinHeight { get; set; }

        /// <summary>
        /// 工作高度最大值
        /// </summary>
        /// <value></value>
        public virtual decimal? MaxHeight { get; set; }

        /// <summary>
        /// 工作风速最小值
        /// </summary>
        /// <value></value>
        public virtual decimal? MinWindSpeed { get; set; }

        /// <summary>
        /// 工作风速最大值
        /// </summary>
        /// <value></value>
        public virtual decimal? MaxWindSpeed { get; set; }

        /// <summary>
        /// 预警限制百分比，默认 90
        /// </summary>
        /// <value></value>
        public virtual decimal? LimitValue { get; set; }

        #endregion 补充
    }
}