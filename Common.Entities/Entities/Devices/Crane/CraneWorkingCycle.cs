﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 工作循环，吊装数据
    /// </summary>

    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneWorkingCycle)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneWorkingCycle : CraneWorkingCycleBase
    {
        public CraneWorkingCycle()
        {
        }

        public CraneWorkingCycle(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 最大记录百分比 float 吊装过程中最大力矩百分比
        /// </summary>
        public virtual decimal? MaxTorquePercent { get; set; }

        /// <summary>
        /// 是否有驾驶员  驾驶舱内是否有驾驶员(true有，false没有)
        /// </summary>
        public virtual bool HasDriver { get; set; }

        /// <summary>
        /// 身份证号    String 驾驶员身份证号
        /// </summary>
        [MaxLength(18)]
        public virtual string IdCard { get; set; }

        /// <summary>
        /// 驾驶员姓名   String 驾驶员姓名
        /// </summary>
        [MaxLength(20)]
        public virtual string DriverName { get; set; }

        /// <summary>
        /// 电源状态    int 电源状态 0正常供电, 1备用电源供电
        /// </summary>
        public virtual int PowerStatus { get; set; }

        /// <summary>
        /// 最大风速 float 本次吊装的最大风速
        /// </summary>
        public virtual decimal? MaxWindSpeed { get; set; }

        /// <summary>
        /// 最小倾角    float 本次吊装的最小倾角(动臂塔机),如没有传递0
        /// </summary>
        public virtual decimal? MinTitl { get; set; }

        /// <summary>
        /// 最大倾角 float 本次吊装的最大倾角(动臂塔机),如没有传递0
        /// </summary>
        public virtual decimal? MaxTitl { get; set; }

        /// <summary>
        /// 本次吊装的状态
        /// [3:0]   起重 0000无，0001重载，0010超载，0011违章，0100故障，其余保留
        /// [7:4]   力矩 0000无，0001重载，0010超载，0011违章，0100故障，其余保留
        /// [11:8]  风速 0000无，0001预警，0010报警，0100故障，其余保留
        /// [15:12] 变幅 0000无，0001预警，0010报警，0100故障，其余保留
        /// [19:16] 高度 0000无，0001预警，0010报警，0100故障，其余保留
        /// [23:20] 塔身倾角 0000无，0010报警，其余保留
        /// [27:24] 保留
        /// </summary>
        public virtual int Status { get; set; }
    }
}