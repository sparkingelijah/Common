﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 驾驶员考勤 /api/dev/tower/upload/tw-driver-attendance
    /// </summary>
    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneDriverAttendance)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneDriverAttendance : CraneDriverAttendanceBase
    {
        public CraneDriverAttendance()
        {
        }

        public CraneDriverAttendance(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 驾驶人号
        /// </summary>
        [MaxLength(50)]
        public virtual string DriverId { get; set; }

        /// <summary>
        /// 考勤设备编号
        /// </summary>
        [MaxLength(50)]
        public virtual string DeviceNo { get; set; }

        /// <summary>
        /// 匹配度
        /// </summary>
        public virtual int? MatchingPercent { get; set; }
    }
}