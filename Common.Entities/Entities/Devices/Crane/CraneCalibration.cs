﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 标定信息
    /// </summary>
    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneCalibration)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneCalibration : CraneCalibrationBase
    {
        public CraneCalibration()
        {
        }

        public CraneCalibration(Guid id) : base(id)
        {
        }
    }
}