﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 报警事件 /api/dev/tower/event/alarm
    /// </summary>

    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneAlarm)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneAlarm : CraneAlarmBase
    {
        public CraneAlarm()
        {
        }

        public CraneAlarm(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 版本号（1byte）
        /// </summary>
        public virtual short Version { get; set; }

        /// <summary>
        /// 警报原因(1byte)
        /// 111 112 113 114 碰撞报警；
        /// 12  13  14  15  重量 力矩 倾斜 风速报警；
        /// 211 212 213 214 碰撞预警；
        /// 22  23  24  25  重量 力矩 倾斜 风速预警；
        /// 101 幅度限位报警
        /// 102 高度限位报警
        /// 103 回转限位报警
        /// 201 幅度限位提醒
        /// 202 高度限位提醒
        /// 203 回转限位提醒
        /// </summary>
        public virtual short AlarmType { get; set; }

        /// <summary>
        /// 事件类型	int 类型 1：报警 2：故障 3：机械限位
        /// </summary>
        public virtual int EventType { get; set; }

        /// <summary>
        /// 报警级别	1-预警，2-报警，3-违章
        /// </summary>
        public virtual int WarnLevel { get; set; }

        /// <summary>
        /// 17:左限位
        /// 18:右限位
        /// 19:近限位
        /// 20:远限位
        /// 21:高限位
        /// 33:左碰撞
        /// 34:右碰撞
        /// 35:近碰撞
        /// 36:远碰撞
        /// 37:低碰撞
        /// 38:左障碍
        /// 39:右障碍
        /// 40:近障碍
        /// 41:远障碍
        /// 53:低障碍
        /// 65:左禁行
        /// 66:右禁行
        /// 81:吊重
        /// 82:力矩
        /// 97:风速
        /// 113:未打卡操作设备
        /// </summary>
        public virtual int WarnType { get; set; }

        /// <summary>
        /// 风速百分比（1byte） 聚正协议中，风速百分比无效
        /// </summary>
        public virtual decimal? WindSpeedPercent { get; set; }

        /// <summary>
        /// 倾斜百分比(1byte)
        /// </summary>
        public virtual decimal? TiltPercent { get; set; }

        /// <summary>
        /// 制动状态（1byte）
        /// 8bit 从低到高分别是 上下前后左右
        /// 1：表示断开
        /// 0：表示闭合
        /// </summary>
        public virtual int BrakingState { get; set; }
    }
}