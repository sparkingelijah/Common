﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{

    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneObstacle)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneObstacle : CraneObstacleBase
    {
        public CraneObstacle()
        {
        }

        public CraneObstacle(Guid id) : base(id)
        {
        }
    }
}