﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 故障事件 /api/dev/tower/event/fault
    /// </summary>

    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneFault)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneFault : CraneFaultBase
    {
        public CraneFault()
        {
        }

        public CraneFault(Guid id) : base(id)
        {
        }
    }
}