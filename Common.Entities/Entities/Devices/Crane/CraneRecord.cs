
using Common.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 塔吊运行数据
    /// </summary>

    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneRecord)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneRecord : CraneRecordBase, IBatchInsertSupport
    {
        public CraneRecord()
        {
        }

        public CraneRecord(Guid id) : base(id)
        {
        }

        #region 基础信息

        /// <summary>
        /// 来源数据id，外部系统跟踪数据用
        /// </summary>
        /// <value></value>
        public virtual string SourceId { get; set; }

        /// <summary>
        /// 安全吊重(吨)
        /// </summary>
        /// <value></value>
        public virtual decimal? SafeLoad { get; set; }

        /// <summary>
        /// 塔机倾角
        /// </summary>
        /// <value></value>
        public virtual decimal? Obliquity { get; set; }

        /// <summary>
        /// 倾斜方向，角度表示
        /// </summary>
        /// <value></value>
        public virtual decimal? DirAngle { get; set; }

        /// <summary>
        /// 预警/报警/违章消息，非正常状态时必须传入
        /// </summary>
        /// <value></value>
        public virtual string Message { get; set; }

        /// <summary>
        /// 驾驶员身份证号
        /// </summary>
        [MaxLength(18)]
        public virtual string IdCard { get; set; }

        /// <summary>
        /// 驾驶员姓名
        /// </summary>
        [MaxLength(20)]
        public virtual string DriverName { get; set; }

        /// <summary>
        /// 电源状态
        /// 0正常供电
        /// 1备用电源供电
        /// </summary>
        public virtual int PowerStatus { get; set; }

        /// <summary>
        /// 本次运行的状态
        /// [3:0]   起重 0000无，0001重载，0010超载，0011违章，0100故障，其余保留
        /// [7:4]   力矩 0000无，0001重载，0010超载，0011违章，0100故障，其余保留
        /// [11:8]  风速 0000无，0001预警，0010报警，0100故障，其余保留
        /// [15:12] 变幅 0000无，0001预警，0010报警，0100故障，其余保留
        /// [19:16] 高度 0000无，0001预警，0010报警，0100故障，其余保留
        /// [23:20] 塔身倾角 0000无，0010报警，其余保留
        /// [27:24] 保留
        /// </summary>
        public virtual int? Status { get; set; }

        /// <summary>
        /// 塔身倾斜度X向与塔机坐标X轴向一致，如安装方向不一致请转换
        /// </summary>
        public virtual decimal? TiltX { get; set; }

        /// <summary>
        /// 塔身倾斜度Y向与塔机坐标Y轴向一致，如安装方向不一致请转换
        /// </summary>
        public virtual decimal? TiltY { get; set; }

        /// <summary>
        /// 故障码
        /// </summary>
        public virtual int? AlarmCode { get; set; }

        #endregion 基础信息

        #region 报警状态

        /// <summary>
        /// 载重预警 0-正常  1-预警  2-报警
        /// </summary>
        /// <value></value>
        public virtual int LoadWarnState { get; set; }

        /// <summary>
        /// 力矩预警 0-正常  1-预警  2-报警
        /// </summary>
        /// <value></value>
        public virtual int KnWarnState { get; set; }

        /// <summary>
        /// 角度预警 0-正常  1-预警  2-报警
        /// </summary>
        /// <value></value>
        public virtual int AngleWarnState { get; set; }

        /// <summary>
        /// 幅度预警 0-正常  1-预警  2-报警
        /// </summary>
        /// <value></value>
        public virtual int RadiusWarnState { get; set; }

        /// <summary>
        /// 高度预警 0-正常  1-预警  2-报警
        /// </summary>
        /// <value></value>
        public virtual int HeightWarnState { get; set; }

        /// <summary>
        /// 风速预警 0-正常  1-预警  2-报警
        /// </summary>
        /// <value></value>
        public virtual int WindSpeedWarnState { get; set; }

        /// <summary>
        /// 回转角度预警 0-正常  1-预警  2-报警
        /// </summary>
        /// <value></value>//
        public virtual int RotationWarnState { get; set; }

        #endregion 报警状态
    }
}