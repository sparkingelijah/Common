﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 保护区信息 /api/dev/tower/upload/tw-reserve-info
    /// </summary>

    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneReserve)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneReserve : CraneReserveBase
    {
        public CraneReserve()
        {
        }

        public CraneReserve(Guid id) : base(id)
        {
        }
    }
}