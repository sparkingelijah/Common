﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{

    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneAlarmSetting)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneAlarmSetting : CraneAlarmSettingBase
    {
        public CraneAlarmSetting()
        {
        }

        public CraneAlarmSetting(Guid id) : base(id)
        {
        }
    }
}