﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 驾驶室抓拍 /api/dev/tower/upload/tw-driver-room-photo
    /// </summary>
    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneCabPhoto)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneCabPhoto : CraneCabPhotoBase
    {
        public CraneCabPhoto()
        {
        }

        public CraneCabPhoto(Guid id) : base(id)
        {
        }
    }
}