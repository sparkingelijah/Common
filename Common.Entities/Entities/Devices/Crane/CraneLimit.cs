﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 限位信息 /api/dev/tower/upload/tw-limit-info
    /// </summary>
    [Table($"{CommonConsts.DevicePrefix}{nameof(CraneLimit)}", Schema = CommonConsts.DeviceSchema)]
    public class CraneLimit : CraneLimitBase
    {
        public CraneLimit()
        {
        }

        public CraneLimit(Guid id) : base(id)
        {
        }
    }
}