﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Common.Entities
{
    /// <summary>
    /// 设备记录的父类
    /// </summary>
    [Description("设备记录父类")]
    public abstract class DeviceRecord : EntityBase
    {
        public DeviceRecord()
        {
        }

        public DeviceRecord(Guid id) : base(id)
        {

        }

        /// <summary>
        /// 设备Id
        /// </summary>
        [Description("设备Id")]
        public virtual Guid? DeviceId { get; set; }

        /// <summary>
        /// 记录采集时间
        /// </summary>
        [Required]
        [Description("记录采集时间")]
        public virtual DateTime RecordTime { get; set; }
    }
}
