﻿using Common.CustomAttributes;
using Common.Enums;
using Common.Extensions;
using Common.Helpers;
using Common.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;

namespace Common.Entities
{
    /// <summary>
    /// 需求
    /// </summary>
    [Description("需求")]
    [Table($"{CommonConsts.DefaultDbTablePrefix}{nameof(Need)}", Schema = CommonConsts.PortalSchema)]
    public class Need : EntityBase
    {
        public Need()
        {
        }
        public Need(Guid id) : base(id)
        {
        }

        #region 基础字段

        /// <summary>
        /// 客户名称
        /// </summary>
        [MaxLength(CommonConsts.MaxLength255), Description("客户名称"), Required]
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [Description("性别")]
        public Sex? Sex { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        [MaxLength(CommonConsts.MaxLength32), Description("手机")]
        public string Phone { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        [MaxLength(CommonConsts.MaxLength32), Description("QQ")]
        public string QNumber { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(CommonConsts.MaxLength128), Description("邮箱")]
        public string Email { get; set; }

        /// <summary>
        /// 需求明细
        /// </summary>
        [MaxLength(CommonConsts.MaxLength2048), Description("需求明细"), Required]
        public string Requirements { get; set; }

        /// <summary>
        /// 是否阅读邮件
        /// </summary>
        [Description("是否阅读邮件")]
        public bool IsRead { get; set; }

        /// <summary>
        /// 阅读邮件时间
        /// </summary>
        [Description("阅读邮件时间")]
        public DateTime? ReadTime { get; set; }

        #endregion

        #region 地址

        /// <summary>
        /// 地址
        /// </summary>
        [Description("地址"), NotSet]
        public virtual Address Address { get; set; } = new Address();

        #endregion

        #region 附件

        /// <summary>
        /// 附件
        /// </summary>
        [Description("附件"), NotSet]
        public virtual HashSet<NeedAttachment> Attachments { get; set; } = new HashSet<NeedAttachment>();

        #endregion


        #region 成员函数

        /// <summary>
        /// 添加附件
        /// </summary>
        public virtual void AddAttachments(IEnumerable<NeedAttachment> attachments)
        {
            foreach (var attachment in attachments)
            {
                //attachment.base.SetEntityPrimaryKey(Guid.NewGuid());
                Attachments.Add(attachment);
            }
        }

        /// <summary>
        /// 删除所有附件
        /// </summary>
        public virtual void RemoveAllAttachments()
        {
            if (Attachments.Any())
            {
                var virtualPath = Attachments.FirstOrDefault()?.Attachment.VirtualPath;
                if (virtualPath.HasValue())
                {
                    //Path.Combine(CustomConfigurationManager.WebRootPath, virtualPath);
                    var targetPath = $@"{EnvironmentHelper.RootPath}{virtualPath}";
                    var dirInfo = new DirectoryInfo(targetPath);
                    if (dirInfo.Parent != null && dirInfo.Parent.Exists)
                    {
                        //删除文件夹要特别谨慎
                        //FileExtension.ClearDir(dirInfo.Parent.FullName);
                        if (Directory.Exists(dirInfo.Parent.FullName))
                        {
                            Directory.Delete(dirInfo.Parent.FullName, true);//删除这个目录及文件
                        }
                    }
                }
                Attachments.RemoveWhere(m => m.NeedId == Id);
            }
        }

        #endregion

    }
}
