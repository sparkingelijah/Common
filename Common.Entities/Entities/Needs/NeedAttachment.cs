﻿using Common.CustomAttributes;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 需求附件表
    /// </summary>
    [Description("需求附件")]
    [Table($"{CommonConsts.DefaultDbTablePrefix}{nameof(NeedAttachment)}", Schema = CommonConsts.PortalSchema)]
    public class NeedAttachment : BaseAttachmentEntity
    {
        public NeedAttachment()
        {
        }

        public NeedAttachment(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 需求外键
        /// </summary>
        [Description("需求外键")]
        public Guid? NeedId { get; set; }

        /// <summary>
        /// 需求实体
        /// </summary>
        [NotSet, Description("需求")]
        public virtual Need Need { get; set; }
    }
}
