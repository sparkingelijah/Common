﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    /// <summary>
    /// 工种
    /// </summary>
    [Description("工种")]
    [Table($"{CommonConsts.DefaultDbTablePrefix}{nameof(WorkerType)}", Schema = CommonConsts.PortalSchema)]
    public class WorkerType : EntityBase
    {
        public WorkerType()
        {
        }
        public WorkerType(Guid id) : base(id)
        {
        }

        /// <summary>
        /// 工种编码
        /// </summary>
        [Description("工种编码")]
        public string Code { get; set; }

        /// <summary>
        /// 工种名称
        /// </summary>
        [Description("工种名称")]
        public string Name { get; set; }
    }
}
