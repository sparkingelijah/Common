﻿using Common.Interfaces;
using StackExchange.Redis;
using System.Threading.Tasks;

namespace Common.Cache
{
    /// <summary>
    /// 自定义reids客户端接口
    /// </summary>
    public interface IServiceExchangeRedisClient : IHandlerType
    {
        /// <summary>
        /// ChangeDb
        /// </summary>
        /// <param name="dbNum"></param>
        Task<IDatabase?> ChangeDb(int dbNum);

        /// <summary>
        /// 获取当前databaseDb
        /// </summary>
        /// <param name="dbNum"></param>
        /// <returns></returns>
        Task<IDatabase> GetRedisClientAsync(int? dbNum = 0);
    }
}
