﻿using Microsoft.Extensions.DependencyInjection;
using Nacos.AspNetCore.V2;
using Nacos.V2;
using Nacos.V2.DependencyInjection;
using Serilog;
using Volo.Abp;
using Volo.Abp.Modularity;

namespace Common.Nacos
{
    /// <summary>
    /// 针对自定义添加dataId 的NacosModule  
    /// 已改为启动项目中使用IHostBuilder.UseNacosConfig配置
    /// 此模块暂不使用，仅保留提供自定义添加Nacos服务参考
    /// </summary>
    public class NacosModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            if (NacosHelper.GetNacosEnabled())
            {
                ConfigureNacos(context);
            }
        }

        /// <summary>
        /// 配置GRPC 集中管理
        /// </summary>
        /// <param name="context"></param>
        private void ConfigureNacos(ServiceConfigurationContext context)
        {
            //Services.AddNacosV2Config自定义配置  NacosSdkOptions
            //注：netcore以后使用启动项目里的 IHostBuilder.UseNacosConfig("NacosConfig",null, x => x.AddSerilog(Log.Logger))方法替换
            //这个方法是直接写在ServiceCollectionExtensions扩展里面的，
            //IHostBuilder.UseNacosConfig的内部实现也是这个 services.AddNacosV2Config
            //且根据appsetting 配置文件里面的指定节点(NacosConfig)批量配置并添加Listeners
            //所以这个NacosModule 没有必要存在了

            context.Services.AddNacosV2Config(options =>
            {
                options.ServerAddresses = new List<string> {
                    NacosHelper.GetNacosUrl()
                };
                options.Namespace = NacosHelper.GetNacosNameSpace();
                options.ConfigUseRpc = true;
            });

            //context.Services.AddNacosAspNet(context.Services.GetConfiguration(), "nacos");
            context.Services.AddNacosAspNet(options =>
            {
                if (!NacosHelper.GetNacosServiceHost().IsNullOrEmpty())
                {
                    options.Ip = NacosHelper.GetNacosServiceHost();
                }
                options.ServiceName = NacosHelper.GetNacosServiceName();
                options.Namespace = NacosHelper.GetNacosNameSpace();
                options.ServerAddresses = new List<string>() { NacosHelper.GetNacosUrl() };
                options.DefaultTimeOut = 15000;
                options.ListenInterval = 1000;
                options.GroupName = NacosHelper.GetNacosGroup();
                options.ClusterName = NacosHelper.GetNacosClusterName();
                string PreferredNetworks = NacosHelper.GetPreferredNetworks();
                if (!string.IsNullOrEmpty(PreferredNetworks))
                {
                    options.PreferredNetworks = PreferredNetworks;
                }
                options.Weight = NacosHelper.GetAppWeight();
                options.NamingUseRpc = NacosHelper.GetUseGrpc();

                var port = NacosHelper.GetAppPort();
                if (port != 0)
                {
                    options.Port = port;
                }

                var serviceId = Guid.NewGuid().ToString();
                // SyncClusterLocal.ClusterCacheServiceId = serviceId;
                options.Metadata = new Dictionary<string, string>() {
                    { "ServiceId",serviceId}
                };
            });

        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            if (NacosHelper.GetNacosEnabled())
            {
                var app = context.GetApplicationBuilder();
                NacosHelper.ApplicationServices = app.ApplicationServices;
                var listener = new NacosEnvironmentVariablesListener();
                //自定义添加一个Listener
                NacosHelper.AddListener(NacosEnvironment.NacosEnvironmentVariablesDataId, NacosHelper.GetNacosGroup(), listener);
            }
        }
    }

    public class NacosEnvironmentVariablesListener : IListener
    {
        public void ReceiveConfigInfo(string configInfo)
        {
            Log.Debug($"收到新配置：NacosEnvironment {configInfo}");
            NacosEnvironment.InitNacosEnvironmentVariables(configInfo);
        }
    }
}